# NewStore code challenge

This repo contains my solution to the Mars Rover TDD Kata by NewStore.

The whole problem description can be found in the [Mars_Rover.pdf](Mars_Rover.pdf).

## Requirements
- NodeJs >= 14.16.0
- npm >= 7.7.6
- Docker (optional)

## Getting started
- Install dependencies
```bash
npm i
```
- Start tests
```
npm test
```

Alternative this repo contains a Dockerfile you can use to create an Docker image and run it. Beware though that this image is not optimized and therefore pretty large. Starting the image will start the tests automatically and remove the container afterwards.
From the root of this repository type:
```bash
# create the Docker image
$ docker build -t lauff-mars-rover .

# start the image 
$ docker run -it --rm lauff-mars-rover
```

## Questions
For this Kata I asked several questions and received the following answers:

### General questions
- Am I free to choose a language to write this challenge in?
    > You free to use the language as long as the language will be (JS, TS, Go)
- The document states "Develop an API..." but what is the actual desired output? A function library? A rest endpoint (with a barebone backend)? A command-line interface?
    > We do not want you to create a rest API. Consider it as a module or class which has functions and return results.  
- Where should the output be written to? The console?
    > We do not need to see the result of the function, you should be sure the results with your unit test because this is a TDD Kata ;)
- Should I use an OOP approach, a functional approach, or should / can I mix them?
    > You can use OOP, functional or whatever you want as long as it is readable and testable. 

### Questions for Part 1
- Can I assume that the rover is always correctly initialized? If not, what is the desired behavior in that case?
    > Yes, you can but it would be good to show if it is correctly initialized with a test case
- Can I assume that the command string only contains valid commands? If not what is the desired behavior of the rover?Should the whole command string be discarded? Should only the invalid commands be discarded/ignored?
    > Yes, you can, you do not need to test command validity.
- Suppose the rover starts at coordinates (0, 0) and can traverse Mars completely by going east for 360 steps. Is the coordinate at the 360th steps 360 or 0? Meaning is the grid we lay on Mars a fixed size, and if not, do I have to account for possible value overflows?
    >  No need to consider these kind of edge cases.

### Questions for Part 2
- Can I assume that the list of obstacles contains only valid tuples?
    > Yes, you can assume
- Edge case: Is it possible to land (initialize) the rover on an obstacle?  If so what is the desired behavior of the rover? From the description, I would assume the rover can operate normally, but I just wanted to make sure.
    > No it is not possible to lang the rover on an obstacle

#### Questions for Part 3
- What is the desired behavior for the rover if the position cannot be reached? Should the rover - like defined in Part 2 - go as far as it can and stop for the obstacle or should it not move it all?
    > Just skip the third part to develop in the challenge.