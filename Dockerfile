FROM node:14.16.1-slim

RUN  apt-get update &&\        
     apt-get install -y git

RUN  git clone https://gitlab.com/mirkolauff/newstore-code-challenge.git
RUN cd newstore-code-challenge

WORKDIR newstore-code-challenge
RUN  npm i --silent

CMD npm run test