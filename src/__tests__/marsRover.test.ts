import {
  createMarsRover,
  moveRover,
  turnRover,
  reportStatus,
  executeCommandString,
  isOnCollisionCourse,
  TDirection,
  TObstacles,
} from '../marsRover'

describe('Mars Rover', () => {
  describe('The mars rover is initialized with its starting coordinates and starting direction', () => {
    it('should correclty initialize the mars rover', () => {
      const marsRover = createMarsRover(4, 2, 'EAST')

      expect(marsRover.x).toBe(4)
      expect(marsRover.y).toBe(2)
      expect(marsRover.direction).toBe('EAST')
    })
  })

  describe('The mars rover moves correctly forward/backwards depending on its direction', () => {
    it('should increase its y-value when moving forward, facing north', () => {
      const marsRover = createMarsRover(0, 0, 'NORTH')
      const movedMarsRover = moveRover(marsRover, 'F')
      const { x, y, direction } = movedMarsRover

      expect([x, y, direction]).toEqual([0, 1, 'NORTH'])
    })

    it('should increase its x-value when moving forward, facing east', () => {
      const marsRover = createMarsRover(0, 0, 'EAST')
      const movedMarsRover = moveRover(marsRover, 'F')
      const { x, y, direction } = movedMarsRover

      expect([x, y, direction]).toEqual([1, 0, 'EAST'])
    })

    it('should reduce its y-value when moving forward, facing south', () => {
      const marsRover = createMarsRover(0, 0, 'SOUTH')
      const movedMarsRover = moveRover(marsRover, 'F')
      const { x, y, direction } = movedMarsRover

      expect([x, y, direction]).toEqual([0, -1, 'SOUTH'])
    })

    it('should reduce its x-value when moving forward, facing west', () => {
      const marsRover = createMarsRover(0, 0, 'WEST')
      const movedMarsRover = moveRover(marsRover, 'F')
      const { x, y, direction } = movedMarsRover

      expect([x, y, direction]).toEqual([-1, 0, 'WEST'])
    })

    it('should reduce its y-value when moving backwards, facing north', () => {
      const marsRover = createMarsRover(0, 0, 'NORTH')
      const movedMarsRover = moveRover(marsRover, 'B')
      const { x, y, direction } = movedMarsRover

      expect([x, y, direction]).toEqual([0, -1, 'NORTH'])
    })

    it('should reduce its x-value when moving backwards, facing east', () => {
      const marsRover = createMarsRover(0, 0, 'EAST')
      const movedMarsRover = moveRover(marsRover, 'B')
      const { x, y, direction } = movedMarsRover

      expect([x, y, direction]).toEqual([-1, 0, 'EAST'])
    })

    it('should increase its y-value when moving backwards, facing south', () => {
      const marsRover = createMarsRover(0, 0, 'SOUTH')
      const movedMarsRover = moveRover(marsRover, 'B')
      const { x, y, direction } = movedMarsRover

      expect([x, y, direction]).toEqual([0, 1, 'SOUTH'])
    })

    it('should increase its x-value when moving backwards, facing west', () => {
      const marsRover = createMarsRover(0, 0, 'WEST')
      const movedMarsRover = moveRover(marsRover, 'B')
      const { x, y, direction } = movedMarsRover

      expect([x, y, direction]).toEqual([1, 0, 'WEST'])
    })
  })

  describe('The mars rover turns correctly depending on its direction', () => {
    it('changes the direction to west when turning left, facing north', () => {
      const marsRover = createMarsRover(0, 0, 'NORTH')
      const movedMarsRover = turnRover(marsRover, 'L')

      expect(movedMarsRover.direction).toBe<TDirection>('WEST')
    })

    it('changes the direction to south when turning left, facing west', () => {
      const marsRover = createMarsRover(0, 0, 'WEST')
      const movedMarsRover = turnRover(marsRover, 'L')

      expect(movedMarsRover.direction).toBe<TDirection>('SOUTH')
    })

    it('changes the direction to east when turning left, facing south', () => {
      const marsRover = createMarsRover(0, 0, 'SOUTH')
      const movedMarsRover = turnRover(marsRover, 'L')

      expect(movedMarsRover.direction).toBe<TDirection>('EAST')
    })

    it('changes the direction to north when turning left, facing east', () => {
      const marsRover = createMarsRover(0, 0, 'EAST')
      const movedMarsRover = turnRover(marsRover, 'L')

      expect(movedMarsRover.direction).toBe<TDirection>('NORTH')
    })

    it('changes the direction to east when turning right, facing north', () => {
      const marsRover = createMarsRover(0, 0, 'NORTH')
      const movedMarsRover = turnRover(marsRover, 'R')

      expect(movedMarsRover.direction).toBe<TDirection>('EAST')
    })

    it('changes the direction to south when turning right, facing east', () => {
      const marsRover = createMarsRover(0, 0, 'EAST')
      const movedMarsRover = turnRover(marsRover, 'R')

      expect(movedMarsRover.direction).toBe<TDirection>('SOUTH')
    })

    it('changes the direction to west when turning right, facing south', () => {
      const marsRover = createMarsRover(0, 0, 'SOUTH')
      const movedMarsRover = turnRover(marsRover, 'R')

      expect(movedMarsRover.direction).toBe<TDirection>('WEST')
    })

    it('changes the direction to north when turning right, facing west', () => {
      const marsRover = createMarsRover(0, 0, 'WEST')
      const movedMarsRover = turnRover(marsRover, 'R')

      expect(movedMarsRover.direction).toBe<TDirection>('NORTH')
    })
  })

  describe('The rover reports its status (position and heading)', () => {
    const marsRover = createMarsRover(4, 2, 'EAST')

    it('should report the current position and heading when the rover did not stop prematurely', () => {
      const status = reportStatus(marsRover, true)

      expect(status).toBe('(4, 2) EAST')
    })

    it('should report that the mars rover has stopped prematurely', () => {
      const status = reportStatus(marsRover, false)

      expect(status).toBe('(4, 2) EAST STOPPED')
    })
  })

  describe('The rover executes the input command string, moves to a new new location and reports', () => {
    it('should execute the whole command string and then report', () => {
      const commandString = 'FLFFFRFLB'
      const marsRover = createMarsRover(4, 2, 'EAST')

      const [rover, report] = executeCommandString(marsRover, commandString)
      const { x, y, direction } = rover

      expect([x, y, direction]).toEqual([6, 4, 'NORTH'])
      expect(report).toBe('(6, 4) NORTH')
    })

    it('should handle an empty command string and keep its position and direction', () => {
      const commandString = ''
      const marsRover = createMarsRover(4, 2, 'EAST')

      const [rover, report] = executeCommandString(marsRover, commandString)
      const { x, y, direction } = rover

      expect([x, y, direction]).toEqual([4, 2, 'EAST'])
      expect(report).toBe('(4, 2) EAST')
    })

    it('should follow the commands until it would collide with an obstacle, stop and report', () => {
      const obstacles: TObstacles = [[3, 5]]
      const commandString = 'FFF'
      const marsRover = createMarsRover(3, 3, 'NORTH')

      const [rover, report] = executeCommandString(
        marsRover,
        commandString,
        obstacles
      )
      const { x, y, direction } = rover

      expect([x, y, direction]).toEqual([3, 4, 'NORTH'])
      expect(report).toBe('(3, 4) NORTH')
    })
  })

  describe('The mars rover should check if its next command would force it to collide with an obstacle', () => {
    const marsRover = createMarsRover(2, 5, 'EAST')
    const obstacles: TObstacles = [
      [1, 4],
      [3, 5],
      [7, 4],
    ]

    it('should return `true` if the next command would make the rover crash', () => {
      const command = 'F'
      const willCollide = isOnCollisionCourse(marsRover, command, obstacles)

      expect(willCollide).toBeTruthy()
    })

    it('should return `false` if the next command would make the rover not crash', () => {
      const command = 'B'
      const willCollide = isOnCollisionCourse(marsRover, command, obstacles)

      expect(willCollide).toBeFalsy()
    })

    it('should return `false` when the rover is turning left', () => {
      const command = 'L'
      const willCollide = isOnCollisionCourse(marsRover, command, obstacles)

      expect(willCollide).toBeFalsy()
    })

    it('hsould return `false` when the rover is turning right', () => {
      const command = 'B'
      const willCollide = isOnCollisionCourse(marsRover, command, obstacles)

      expect(willCollide).toBeFalsy()
    })
  })
})
