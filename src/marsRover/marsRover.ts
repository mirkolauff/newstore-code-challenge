import * as R from 'ramda'
import {
  TDirection,
  TMarsRover,
  TCommand,
  TCommands,
  TMoveCommand,
  TMovementDirectionKey,
  TDirectionModifier,
  TTurnCommand,
  TTurnDirectionKey,
  ICreateReportInput,
  TMarsRoverStausReportPair,
  TExecuteCommandsAccumulator,
  TObstacles,
} from './metadata'

const MappingMovementDirectionToModifier = new Map<
  TMovementDirectionKey,
  TDirectionModifier
>([
  ['F_NORTH', [0, 1]],
  ['F_EAST', [1, 0]],
  ['F_SOUTH', [0, -1]],
  ['F_WEST', [-1, 0]],
  ['B_NORTH', [0, -1]],
  ['B_EAST', [-1, 0]],
  ['B_SOUTH', [0, 1]],
  ['B_WEST', [1, 0]],
])

const MappingTurnDirectionToDirection = new Map<TTurnDirectionKey, TDirection>([
  ['L_NORTH', 'WEST'],
  ['L_WEST', 'SOUTH'],
  ['L_SOUTH', 'EAST'],
  ['L_EAST', 'NORTH'],
  ['R_NORTH', 'EAST'],
  ['R_WEST', 'NORTH'],
  ['R_SOUTH', 'WEST'],
  ['R_EAST', 'SOUTH'],
])

export const createMarsRover = (
  x: number,
  y: number,
  direction: TDirection
): TMarsRover => ({
  x,
  y,
  direction,
})

const getMovementModifier = (marsRover: TMarsRover, command: TMoveCommand) => {
  const key = `${command}_${marsRover.direction}` as const
  return MappingMovementDirectionToModifier.get(key)!
}

const getMarsRoverNextPosition = (
  marsRover: TMarsRover,
  modifier: TDirectionModifier
) => {
  const { x, y } = marsRover
  const [deltaX, deltaY] = modifier

  return [x + deltaX, y + deltaY]
}

export const moveRover = (marsRover: TMarsRover, command: TMoveCommand) => {
  const modifier = getMovementModifier(marsRover, command)
  const [x, y] = getMarsRoverNextPosition(marsRover, modifier)

  return createMarsRover(x, y, marsRover.direction)
}

export const turnRover = (marsRover: TMarsRover, command: TTurnCommand) => {
  const key = `${command}_${marsRover.direction}` as const
  const direction = MappingTurnDirectionToDirection.get(key)!

  return createMarsRover(marsRover.x, marsRover.y, direction)
}

export const reportStatus = (marsRover: TMarsRover, hasCompleted: boolean) => {
  const { x, y, direction } = marsRover
  const defaultStatusString = `(${x}, ${y}) ${direction}`
  const optionalString = hasCompleted ? '' : ' STOPPED'

  return defaultStatusString + optionalString
}

const splitCommandString = (commandString: string) => commandString.split('')

const createReport = (input: ICreateReportInput): TMarsRoverStausReportPair => {
  const { marsRover, hasCompleted } = input
  return [marsRover, reportStatus(marsRover, hasCompleted)]
}

const isMoveCommand = (
  command: TMoveCommand | TTurnCommand
): command is TMoveCommand => {
  return ['F', 'B'].includes(command)
}

const executeCommandWithObstaclesReducer = (obstacles: TObstacles) => (
  acc: TExecuteCommandsAccumulator,
  command: TCommand
): TExecuteCommandsAccumulator | R.Reduced<TExecuteCommandsAccumulator> => {
  if (isOnCollisionCourse(acc.marsRover, command, obstacles))
    return R.reduced(acc)

  const marsRover = isMoveCommand(command)
    ? moveRover(acc.marsRover, command)
    : turnRover(acc.marsRover, command)

  const numCommands = acc.numCommands + 1

  return { marsRover, numCommands }
}

const executeCommands = R.curry(
  (
    marsRover: TMarsRover,
    obstacles: TObstacles,
    commands: TCommands
  ): ICreateReportInput => {
    const accumulator: TExecuteCommandsAccumulator = {
      marsRover,
      numCommands: 0,
    }

    const result = R.reduce(
      executeCommandWithObstaclesReducer(obstacles),
      accumulator,
      commands
    )

    return { marsRover: result.marsRover, hasCompleted: true }
  }
)

export const executeCommandString = (
  marsRover: TMarsRover,
  commandString: string,
  obstacles: TObstacles = []
) => {
  return R.pipe(
    splitCommandString,
    executeCommands(marsRover, obstacles),
    createReport
  )(commandString)
}

export const isOnCollisionCourse = (
  marsRover: TMarsRover,
  command: TCommand,
  obstacles: TObstacles
) => {
  if (!isMoveCommand(command)) return false

  const modifier = getMovementModifier(marsRover, command)
  const nextPosition = getMarsRoverNextPosition(marsRover, modifier)

  const nextPositionString = JSON.stringify(nextPosition)
  const obstacleString = JSON.stringify(obstacles)

  return obstacleString.includes(nextPositionString)
}
