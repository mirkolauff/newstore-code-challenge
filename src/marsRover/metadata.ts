export type TDirection = 'NORTH' | 'EAST' | 'SOUTH' | 'WEST'
export type TMarsRover = {
  x: number
  y: number
  direction: TDirection
}
export type TMoveCommand = 'F' | 'B'
export type TTurnCommand = 'L' | 'R'
export type TCommand = TMoveCommand | TTurnCommand
export type TCommands = Array<TCommand>
export type NumberPair = [number, number]
export type TDirectionModifier = NumberPair
export type TMovementDirectionKey = `${TMoveCommand}_${TDirection}`
export type TTurnDirectionKey = `${TTurnCommand}_${TDirection}`
export interface ICreateReportInput {
  marsRover: TMarsRover
  hasCompleted: boolean
}
export type TMarsRoverStausReportPair = [TMarsRover, string]
export type TExecuteCommandsAccumulator = {
  marsRover: TMarsRover
  numCommands: number
}
export type TObstacle = NumberPair
export type TObstacles = Array<TObstacle>
